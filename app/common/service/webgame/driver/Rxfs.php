<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\service\webgame\driver;

use app\common\service\Webgame\Driver;
use app\common\service\Webgame;

/**
 * 热血封神页游服务驱动
 */
class Rxfs extends Webgame implements Driver
{
    
    /**
     * 驱动基本信息
     */
    public function driverInfo()
    {
        
        return ['driver_name' => '热血封神驱动', 'driver_class' => 'Rxfs', 'driver_describe' => '热血封神页游驱动', 'author' => 'Bigotry', 'version' => '1.0'];
    }
    
    /**
     * 获取驱动参数
     */
    public function getDriverParam()
    {
        
        return ['login_key' => '热血封神登录KEY', 'pay_key' => '热血封神充值KEY'];
    }
    
    /**
     * 获取配置信息
     */
    public function config()
    {
        
        return $this->driverConfig('Rxfs');
    }
    
    /**
     * 热血封神登录
     */
    public function login($mid = 0, $sid = 0, $is_client = 0)
    {
        
    }
    
    /**
     * 热血封神角色列表
     */
    public function roles($mid, $sid)
    {
        
    }
   
    /**
     * 热血封神充值
     */
    public function pay($order)
    {
        
    }
}
