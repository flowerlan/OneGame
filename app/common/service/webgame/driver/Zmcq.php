<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\service\webgame\driver;

use app\common\service\Webgame\Driver;
use app\common\service\Webgame;

/**
 * 战魔传奇页游服务驱动
 */
class Zmcq extends Webgame implements Driver
{
    
    /**
     * 驱动基本信息
     */
    public function driverInfo()
    {
        
        return ['driver_name' => '战魔传奇驱动', 'driver_class' => 'Zmcq', 'driver_describe' => '战魔传奇页游驱动', 'author' => 'Bigotry', 'version' => '1.0'];
    }
    
    /**
     * 获取驱动参数
     */
    public function getDriverParam()
    {
        
        return ['login_key' => '战魔传奇登录KEY', 'pay_key' => '战魔传奇充值KEY'];
    }
    
    /**
     * 获取配置信息
     */
    public function config()
    {
        
        return $this->driverConfig('Zmcq');
    }
    
    /**
     * 战魔传奇登录
     */
    public function login($mid = 0, $sid = 0, $is_client = 0)
    {
        
    }
    
    /**
     * 战魔传奇角色列表
     */
    public function roles($mid, $sid)
    {
        
    }
   
    /**
     * 战魔传奇充值
     */
    public function pay($order)
    {
        
    }
}
