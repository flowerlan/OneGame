<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\logic;

/**
 * 公会相关逻辑
 */
class Conference extends AdminBase
{
    
    /**
     * 获取公会列表
     */
    public function getConferenceList($where = [], $field = 'c.*,mm.nickname as m_nickname,ms.nickname as s_nickname', $order = 'c.create_time desc', $paginate = 0)
    {
        
        $this->modelWgConference->alias('c');
        
        $join = [
                    [SYS_DB_PREFIX . 'member mm', 'mm.id = c.member_id'],
                    [SYS_DB_PREFIX . 'member ms', 'ms.id = c.source_member_id'],
                ];
        
        $where['c.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        return $this->modelWgConference->getList($where, $field, $order, $paginate, $join);
    }
    
    /**
     * 获取员工列表
     */
    public function getEmployeeList()
    {
        
        $this->modelWgConferenceMember->alias('cm');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_conference c', 'c.id = cm.conference_id'],
                    [SYS_DB_PREFIX . 'member m', 'm.id = cm.member_id'],
                ];
        
        $where['cm.' . DATA_STATUS_NAME]    = ['neq', DATA_DELETE];
        
        $field = 'c.conference_name,cm.*,m.nickname,m.username,m.mobile';
        
        return $this->modelWgConferenceMember->getList($where, $field, 'cm.create_time desc', DB_LIST_ROWS, $join);
    }
    
    /**
     * 公会添加
     */
    public function conferenceAdd($data = [])
    {
        
        if (!$this->validateConference->scene('add')->check($data)) {
            
            return [RESULT_ERROR, $this->validateConference->getError()];
        }
        
        $func = function () use ($data) {
            
                    $member['nickname']         = $member['username'] = $data['username'];
                    $member['password']         = data_md5_key($data['password']);
                    $member['leader_id']        = MEMBER_ID;
                    $member['is_inside']        = DATA_NORMAL;
                    
                    $data['member_id']          = $this->modelMember->insertGetId($member);
                    $data['source_member_id']   = MEMBER_ID;
                    
                    action_log('新增', '新增会员，username：' . $member['username']);
            
                    $this->modelWgConference->setInfo($data) && action_log('新增', '新增公会，conference_name：' . $data['conference_name']);
                    
                };
        
        return closure_list_exe([$func]) ? [RESULT_SUCCESS, '操作成功', url('conferenceList')] : [RESULT_ERROR, '公会添加失败'];
    }
    
    /**
     * 公会编辑
     */
    public function conferenceEdit($data = [])
    {
        
        if (!$this->validateConference->scene('edit')->check($data)) {
            
            return [RESULT_ERROR, $this->validateConference->getError()];
        }
        
        $result = $this->modelWgConference->setInfo($data);
        
        $result && action_log('编辑', '编辑公会，conference_name：' . $data['conference_name']);
            
        return $result ? [RESULT_SUCCESS, '操作成功', url('conferenceList')] : [RESULT_ERROR, $this->modelWgConference->getError()];
    }
    
    /**
     * 获取公会信息
     */
    public function getConferenceInfo($where = [], $field = true)
    {
        
        return $this->modelWgConference->getInfo($where, $field);
    }
    
    /**
     * 员工新增
     */
    public function employeeAdd($data = [])
    {
        
        if (!$this->validateEmployee->scene('add')->check($data)) {
            
            return [RESULT_ERROR, $this->validateEmployee->getError()];
        }
        
        $func = function () use ($data) {
            
                    $member['nickname']         = $member['username'] = $data['username'];
                    $member['password']         = data_md5_key($data['password']);
                    $member['leader_id']        = MEMBER_ID;
                    $member['is_inside']        = DATA_NORMAL;
                    
                    !empty($data['mobile']) && $member['mobile'] = $data['mobile'];
                    
                    $conference_data['member_id']          = $this->modelMember->insertGetId($member);
                    $conference_data['conference_id']      = $data['conference_id'];
                    
                    action_log('新增', '新增会员，username：' . $member['username']);
            
                    $this->modelWgConferenceMember->setInfo($conference_data) && action_log('新增', '新增公会员工，username：' . $member['username']);
                    
                };
        
        return closure_list_exe([$func]) ? [RESULT_SUCCESS, '操作成功', url('employeeList')] : [RESULT_ERROR, '员工添加失败'];
    }
    
    
    /**
     * 获取链接列表
     */
    public function getLinkList()
    {
        
        $this->modelWgCode->alias('wcd');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_conference wcf', 'wcd.conference_id = wcf.id'],
                    [SYS_DB_PREFIX . 'member m', 'm.id = wcd.member_id'],
                    [SYS_DB_PREFIX . 'wg_game g', 'g.id = wcd.game_id'],
                ];
        
        $where['wcd.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $field = 'wcf.conference_name,m.username,g.game_name,wcd.*';
        
        return $this->modelWgCode->getList($where, $field, 'wcd.create_time desc', DB_LIST_ROWS, $join);
    }
    
    /**
     * 链接新增
     */
    public function linkAdd($data = [])
    {
        
        if (empty($data['conference_id']) || empty($data['member_id'])) {
            
            return [RESULT_ERROR, '请检查公会或员工是否选择'];
        }
        
        $check_info = $this->modelWgCode->getInfo($data);
        
        if (!empty($check_info)) {
            
            return [RESULT_ERROR, '员工游戏链接已存在'];
        }
        
        begin:
        
        $code = rand_code(8);
        
        $info = $this->modelWgCode->getInfo(['code' => $code]);
        
        if (!empty($info)) {
            
            goto begin;
        }
            
        $data['code']   = $code;
        $data['status'] = DATA_NORMAL;

        $result = $this->modelWgCode->setInfo($data);
        
        $result && action_log('新增', '新增链接，code：' . $code);
            
        return $result ? [RESULT_SUCCESS, '操作成功', url('linkList')] : [RESULT_ERROR, $this->modelWgCode->getError()];
    }
    
    /**
     * 获取员工选择项文本
     */
    public function getEmployeeOptions($where = [])
    {
        
        $this->modelWgConferenceMember->alias('cm');
        
        $join = [
                    [SYS_DB_PREFIX . 'member m', 'm.id = cm.member_id'],
                ];
        
        $where['cm.' . DATA_STATUS_NAME]    = ['neq', DATA_DELETE];
        
        $field = 'cm.*,m.username';
        
        $list = $this->modelWgConferenceMember->getList($where, $field, 'cm.create_time desc', false, $join);
        
        $text = '<option value="0">—请选择员工—</option>';
        
        foreach ($list as $info)
        {
            
            $text .= '<option value="'.$info['member_id'].'">' . $info['username'] . '</option>';
        }
        
        return $text;
    }
    
    /**
     * 获取绑定列表
     */
    public function getBindList()
    {
        
        $this->modelWgBind->alias('wb');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_conference wc', 'wb.conference_id = wc.id'],
                    [SYS_DB_PREFIX . 'wg_game g', 'g.id = wb.game_id'],
                    [SYS_DB_PREFIX . 'member m1', 'm1.id = wb.member_id'],
                    [SYS_DB_PREFIX . 'member m2', 'm2.id = wb.employee_id']
                ];
        
        $where['wb.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $field = 'wc.conference_name,g.game_name,m2.username as employee_username,m1.username,wb.*';
        
        return $this->modelWgBind->getList($where, $field, 'wb.create_time desc', DB_LIST_ROWS, $join);
    }
    
    /**
     * 绑定新增
     */
    public function bindAdd($data = [])
    {
        
        if (empty($data['conference_id']) || empty($data['employee_id'])) { return [RESULT_ERROR, '请检查公会或员工是否选择']; }
        
        if (empty($data['username'])) { return [RESULT_ERROR, '请输入需要绑定的玩家账号']; }
        
        $member_info = $this->modelMember->getInfo(['username' => $data['username']]);
        
        if (empty($member_info)) { return [RESULT_ERROR, '玩家账号不存在']; }
        
        unset($data['username']);
        
        $data['member_id'] = $member_info['id'];
        
        $bind_info = $this->modelWgBind->getInfo($data);
        
        if (!empty($bind_info)) { return [RESULT_ERROR, '绑定信息已存在']; }
            
        $data['status'] = DATA_NORMAL;

        $result = $this->modelWgBind->setInfo($data);
        
        $result && action_log('新增', '新增绑定，username：' . $member_info['username']);
            
        return $result ? [RESULT_SUCCESS, '操作成功', url('bindList')] : [RESULT_ERROR, $this->modelWgBind->getError()];
    }
}
