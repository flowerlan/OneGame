<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\controller;

/**
 * 公会相关控制器
 */
class Conference extends AdminBase
{
    
    /**
     * 公会列表
     */
    public function conferenceList()
    {
        
        $this->assign('list', $this->logicConference->getConferenceList());
        
        return $this->fetch('conference_list');
    }
    
    /**
     * 公会添加
     */
    public function conferenceAdd()
    {
        
        IS_POST && $this->jump($this->logicConference->conferenceAdd($this->param));
        
        return $this->fetch('conference_add');
    }
    
    /**
     * 公会编辑
     */
    public function conferenceEdit()
    {
        
        IS_POST && $this->jump($this->logicConference->conferenceEdit($this->param));
        
        !empty($this->param['id']) && $this->assign('info', $this->logicConference->getConferenceInfo(['id' => $this->param['id']]));
        
        return $this->fetch('conference_edit');
    }
    
    /**
     * 员工列表
     */
    public function employeeList()
    {
        
        $this->assign('list', $this->logicConference->getEmployeeList());
        
        return $this->fetch('employee_list');
    }
    
    /**
     * 员工添加
     */
    public function employeeAdd()
    {
        
        IS_POST && $this->jump($this->logicConference->employeeAdd($this->param));
        
        $this->assign('conference_list', $this->logicConference->getConferenceList());
        
        return $this->fetch('employee_add');
    }
    
    /**
     * 链接列表
     */
    public function linkList()
    {
        
        $this->assign('list', $this->logicConference->getLinkList());
        
        return $this->fetch('link_list');
    }
    
    /**
     * 链接添加
     */
    public function linkAdd()
    {
        
        IS_POST && $this->jump($this->logicConference->linkAdd($this->param));
        
        $this->assign('conference_list', $this->logicConference->getConferenceList());
        
        $this->assign('game_list', $this->logicGame->getGameList());
        
        return $this->fetch('link_add');
    }
    
    /**
     * 获取员工选择项
     */
    public function getEmployeeOptions($conference_id = 0)
    {
        
        $where['cm.conference_id'] = $conference_id;
        
        $data['content'] = $this->logicConference->getEmployeeOptions($where);
        
        return $data;
    }
    
    /**
     * 绑定列表
     */
    public function bindList()
    {
        
        $this->assign('list', $this->logicConference->getBindList());
        
        return $this->fetch('bind_list');
    }
    
    /**
     * 绑定添加
     */
    public function bindAdd()
    {
        
        IS_POST && $this->jump($this->logicConference->bindAdd($this->param));
        
        $this->assign('conference_list', $this->logicConference->getConferenceList());
        
        $this->assign('game_list', $this->logicGame->getGameList());
        
        return $this->fetch('bind_add');
    }
}
