<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

 /**
  * 记录执行URL
  */
function set_url($default_url = '')
{
    
    $url = empty($default_url) ? $_SERVER['REQUEST_URI'] : $default_url;
    
    (defined('URL') && in_array(URL, config('forward_url_list'))) && cookie('__forward__', $url);
}

 /**
  * 获取记录URL
  */
function get_url($default_url = '')
{
    
    $url = cookie('__forward__');
    
    if (!empty($url)) {
        
        cookie('__forward__', null);
        
        return $url;
    }
    
    return $default_url;
}