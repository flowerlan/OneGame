<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\controller;

/**
 * 登录注册控制器
 */
class Login extends IndexBase
{
    
    /**
     * 登录
     */
    public function login()
    {
        
        is_login() && $this->redirect('index/index');

        $this->assign('title', '用户登录');

        return $this->fetch(); // 渲染模板 
    }
    
    /**
     * 登录处理
     */
    public function loginHandle()
    {
        
        $this->jump($this->logicLogin->loginHandle($this->param));
    }
    
    /**
     * 注册
     */
    public function register()
    {
        
        is_login() && $this->redirect('index/index');

        $this->assign('title', '用户注册');

        return $this->fetch(); // 渲染模板 
    }
    
    /**
     * 注册处理
     */
    public function registerHandle()
    {
        
        $this->jump($this->logicLogin->registerHandle($this->param));
    }
    
    /**
     * 退出
     */
    public function logout()
    {
        
        $this->logicLogin->logout();
        
        $this->redirect('login/login');
    }
}
