<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\controller;

/**
 * 前台首页控制器
 */
class Index extends IndexBase
{
    
    // 首页方法
    public function index()
    {
        
        set_url();
        
        $this->assign('data', $this->logicIndex->getIndexData());
        
        return $this->fetch('index');
    }
    
    // 游戏推广渠道页面
    public function channel($code = '')
    {
        
        dump($code);
    }
}
