<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\logic;

/**
 * 首页逻辑
 */
class Index extends IndexBase
{
    
    /**
     * 获取首页数据
     */
    public function getIndexData()
    {
        
        // 轮播
        $data['slider_list']            =   $this->getSlider();
        
        // 新闻资讯
        $data['article_list']           =   $this->getArticle();
        
        // 最近新服
        $data['server_list']            =   $this->getServer();
        
        // 推荐游戏
        $data['recommend_game_list']    =   $this->getRecommendGame();
        
        // 更多游戏
        $data['more_game_list']         =   $this->getMoreGame();
        
        // 游戏礼包
        $data['gift_list']              =   $this->getGift();
        
        // 最近玩过
        $data['play_list']              =   $this->getPlay();
        
        return $data;
    }
    
    /**
     * 轮播
     */
    public function getSlider()
    {
        
        return $this->modelSlider->getList([], true, 'sort desc', false);
    }
    
    /**
     * 新闻资讯
     */
    public function getArticle()
    {
        
        $this->modelArticle->alias('a');
        
        $join = [
                    [SYS_DB_PREFIX . 'article_category c', 'a.category_id = c.id'],
                ];
        
        $where['a.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $field = 'a.id,a.name,c.name as category_name';
        
        return $this->modelArticle->getList($where, $field, 'a.create_time desc', false, $join, null, 9);
    }
    
    /**
     * 最近新服
     */
    public function getServer()
    {
        
        $this->modelWgServer->alias('s');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_game g', 's.game_id = g.id'],
                ];
        
        $where['s.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        $where['s.start_time']          = ['elt', TIME_NOW];
        $where['s.maintain_end_time']   = ['elt', TIME_NOW];
        
        $field = 'g.game_logo,g.game_name,g.game_code,s.server_name,s.start_time,s.cp_server_id';
        
        return $this->modelWgServer->getList($where, $field, 's.start_time desc', false, $join, null, 7);
    }
    
    /**
     * 推荐游戏
     */
    public function getRecommendGame()
    {
        
        $where[DATA_STATUS_NAME]      = ['neq', DATA_DELETE];
        $where['maintain_end_time']   = ['elt', TIME_NOW];
        $where['is_recommend']        = DATA_NORMAL;
        
        return $this->modelWgGame->getList($where, 'id,game_name,game_code,game_cover', 'sort desc', false, [], null, 2);
    }
    
    /**
     * 更多游戏
     */
    public function getMoreGame()
    {
        
        $this->modelWgGame->alias('g');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_category c', 'g.game_category_id = c.id'],
                ];
        
        $where['g.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        $where['g.maintain_end_time']   = ['elt', TIME_NOW];
        $where['g.is_hot']              = ['neq', DATA_NORMAL];
        
        $field = 'g.game_logo,g.game_head,g.game_name,g.game_code,c.category_name';
        
        return $this->modelWgGame->getList($where, $field, 'g.sort desc', false, $join, null, 6);
    }
    
    /**
     * 游戏礼包
     */
    public function getGift()
    {
        
        $this->modelWgGift->alias('gi');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_game ga', 'ga.id = gi.game_id'],
                ];
        
        $where['gi.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $field = 'gi.id,gi.gift_name,gi.create_time,ga.game_logo,ga.game_head';
        
        $list = $this->modelWgGift->getList($where, $field, 'gi.create_time desc', false, $join, null, 6);
        
        $list_ids = array_extract($list);
        
        $key_where['is_get']   = DATA_DISABLE;
        $key_where['gift_id']  = ['in', $list_ids];
        
        $list_number = $this->modelWgGiftKey->where($key_where)->group("gift_id")->field("gift_id,count('id') as number")->select();
        
        foreach ($list as &$info)
        {
            foreach ($list_number as $number_info)
            {
                $info['id'] != $number_info['gift_id'] ?: $info['number'] = $number_info['number'];
            }
        }
        
        return $list;
    }
    
    /**
     * 最近玩过
     */
    public function getPlay()
    {
        
        $member_id = is_login();
        
        if (empty($member_id)) {
            
            return [];
        }
        
        $this->modelWgRole->alias('wr');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_game ga', 'ga.id = wr.game_id'],
                    [SYS_DB_PREFIX . 'wg_server ws', 'ws.id = wr.server_id'],
                ];
        
        $where['wr.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        $where['wr.member_id'] = $member_id;
        
        $field = 'wr.id,ga.game_logo,ga.game_name,ga.game_code,ws.server_name,ws.cp_server_id';
        
        $list = $this->modelWgRole->getList($where, $field, 'wr.update_time desc', false, $join, null, 7);
        
        return $list;
    }
}
